﻿google.charts.load('current', { 'packages': ['corechart'] });
function drawSpeedGraph1m(idHost, range = 60) {
    var options = {
        title: 'Last ' + range / 60 + ' minute speed',
        curveType: 'function',
        legend: { position: 'bottom' },
        vAxis: { format: '# Mbit/s', viewWindow: { min: 0 } }
        // vAxis: { viewWindowMode: "explicit", viewWindow: { min: 0 } }
    };

    $.ajax({
        type: "POST",
        url: "stats.aspx/GetChartSpeedsDataSeconds",
        data: '{ id: "' + idHost + '", range_seconds: ' + range + ' }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var data = google.visualization.arrayToDataTable(r.d);
            var chart = new google.visualization.LineChart(document.getElementById('curvechart1m'));
            
            chart.draw(data, options);
        },
        failure: function (r) {
            //alert('drawSpeedGraph1m' + r.d);
        },
        error: function (r) {
            //alert('drawSpeedGraph1m' + r.d);
        }
    });
}
    function drawCurrentNetowrkUsage(idHost) {

        $.ajax({
            type: "POST",
            url: "stats.aspx/GetCurrentNetworkUsage",
            data: '{ id: "' + idHost + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                var text = document.getElementById('network_usage');
                var text1 = document.getElementById('network_usage_total');
                if (text !== null) {
                    text.innerHTML = '<i class="fa fa-arrow-down" aria-hidden="true"></i>' + Math.round(r.d[0] * 8 / 10000.0) / 100.0
                        + ' Mbit/s  <i class="fa fa-arrow-up" aria-hidden="true"></i>' + Math.round(r.d[1] * 8 / 10000.0) / 100.0 + ' Mbit/s';
                }
                if(text1 !== null) {
                    text1.innerHTML = 'Total: <i class="fa fa-arrow-down" aria-hidden="true"></i>' + Math.round(r.d[2] / (1024.0 * 1024.0))
                        + ' MB  <i class="fa fa-arrow-up" aria-hidden="true"></i>' + Math.round(r.d[3] / (1024.0 * 1024.0)) + ' MB';
                }
            },
            failure: function (r) {
                //alert('drawCurrentNetowrkUsage 1 ' + r.d);
            },
            error: function (r) {
                //alert('drawCurrentNetowrkUsage ' + r.d);
            }
        });


    }


    function drawSpeedGraph24h(idHost, range = 7200) {
        var options = {
            title: 'Last ' + range / (60*60) +' hours speed',
            curveType: 'function',
            legend: { position: 'bottom' },
            vAxis: { format: '# Mbit/s', viewWindow: { min: 0 } }
            // vAxis: { viewWindowMode: "explicit", viewWindow: { min: 0 } }
        };

        $.ajax({
            type: "POST",
            url: "stats.aspx/GetChartSpeedsDataMinute",
            data: '{ id: "' + idHost + '", range_seconds: ' + range + ' }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                var data = google.visualization.arrayToDataTable(r.d);
                var chart = new google.visualization.LineChart(document.getElementById('curvechart24h'));
                chart.draw(data, options);
            },
            failure: function (r) {
                //alert('drawSpeedGraph24h' + r.d);
            },
            error: function (r) {
                //alert('drawSpeedGraph24h' + r.d);
            }
        });
    }
        function drawPiePort(idHost) {
            var options = {
                title: '',
                pieStartAngle: 200,
                pieHole: 0.00,
                slices: {
                    0: { offset: 0.05 }
                }
            };

            $.ajax({
                type: "POST",
                url: "stats.aspx/GetChartPortsData",
                data: '{ id: "' + idHost + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    var data = google.visualization.arrayToDataTable(r.d);
                    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                    chart.draw(data, options);
                },
                failure: function (r) {
                    //alert('drawPiePort' + r.d);
                },
                error: function (r) {
                    //alert('drawPiePort' + r.d);
                }
            });
        }