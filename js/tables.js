﻿google.charts.load('current', { 'packages': ['table'] });
function drawHostsTable() {

    $.ajax({
        type: "POST",
        url: "hosts.aspx/GetLastQueryDnsTableData",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var data = google.visualization.arrayToDataTable(r.d);
            var table = new google.visualization.Table(document.getElementById('hosts_table'));

            table.draw(data, { width: '100%', height: '100%' });
        },
        failure: function (r) {
            //alert('asd' + r.d);
        },
        error: function (r) {
            //alert('asd' + r.d);
        }
    });
}


function drawLastQueryDnsTable(idHost, limit = 5) {

    $.ajax({
        type: "POST",
        url: "stats.aspx/GetLastQueryDnsTableData",
        data: '{ id: "' + idHost + '", limit: ' + limit + ' }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var data = google.visualization.arrayToDataTable(r.d);
            var table = new google.visualization.Table(document.getElementById('last_query_dns'));
            google.visualization.events.addListener(table, 'ready', changeTableStyle);
            table.draw(data, {  allowHtml: true, width: '100%', height: '100%' });
        },
        failure: function (r) {
            //alert('drawLastQueryDnsTable' + r.d);
        },
        error: function (r) {
            //alert('drawLastQueryDnsTable' + r.d);
        }
    });
}
function drawLastVisitedSitesTable(idHost, limit = 5) {

    $.ajax({
        type: "POST",
        url: "stats.aspx/GetLastVisitedSitesTableData",
        data: '{ id: "' + idHost + '", limit: ' + limit + ' }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var data = google.visualization.arrayToDataTable(r.d);
            var table = new google.visualization.Table(document.getElementById('last_visited_sites'));
            google.visualization.events.addListener(table, 'ready', changeTableStyle);
            table.draw(data, {  width: '100%', height: '100%' });
        },
        failure: function (r) {
            //alert('drawLastVisitedSitesTable' + r.d);
        },
        error: function (r) {
            //alert('drawLastVisitedSitesTable' + r.d);
        }
    });
}
function drawBindingsTable(idHost, limit = 5) {
    $.ajax({
        type: "POST",
        url: "stats.aspx/GetBindingTableData",
        data: '{ id: "' + idHost + '", limit: ' + limit + ' }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var data = google.visualization.arrayToDataTable(r.d);
            var table = new google.visualization.Table(document.getElementById('binding_table'));
            google.visualization.events.addListener(table, 'ready', changeTableStyle);
            table.draw(data, {width: '100%', height: '100%' });
        },
        failure: function (r) {
            //alert('drawBindingsTable' + r.d);
        },
        error: function (r) {
            //alert('drawBindingsTable' + r.d);
        }
    });
}
function drawLastContacts(idHost, limit = 5) {
    $.ajax({
        type: "POST",
        url: "stats.aspx/GetLastContactsTableData",
        data: '{ id: "' + idHost + '", limit: ' + limit + ' }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var data = google.visualization.arrayToDataTable(r.d);
            var table = new google.visualization.Table(document.getElementById('last_contacts_table'));
            google.visualization.events.addListener(table, 'ready', changeTableStyle);
            table.draw(data, { width: '100%', height: '100%' });
        },
        failure: function (r) {
            //alert('drawLastContacts' + r.d);
        },
        error: function (r) {
            //alert('drawLastContacts' + r.d);
        }
    });
}

function changeTableStyle() {
    $(".google-visualization-table-table").find('*').each(function (i, e) {
        var classList = e.className ? e.className.split(/\s+/) : [];
        $.each(classList, function (index, item) {
            if (item.indexOf("google-visualization") === 0) {
                $(e).removeClass(item);
            }
        });
    });

    $(".google-visualization-table-table")
        .removeClass('google-visualization-table-table')
        .addClass('table table-striped table-hover table-hover')
        .css("width", "");
}