﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Descrizione di riepilogo per DbReader
/// </summary>
/// 

public class QueryParameter
{
    public string Name;
    public object Value;
    public SqlDbType Type;
}
public class DbConnection
{
    private SqlConnection _conn;
    public DbConnection(string connectionString)
    {
        _conn = new SqlConnection(connectionString);
        _conn.Open();
    }
    private SqlCommand GetCommand(string query, params QueryParameter[] queryParams)
    {
        SqlCommand command = new SqlCommand(query, _conn);
        foreach (QueryParameter qp in queryParams)
        {
            command.Parameters.Add("@" + qp.Name, qp.Type);
            command.Parameters["@" + qp.Name].Value = qp.Value;
        }
        return command;
    }
    public SqlDataReader Read(string query, params QueryParameter[] queryParams)
    {
        SqlCommand command = GetCommand(query, queryParams);
        SqlDataReader reader = command.ExecuteReader();
        command.Dispose(); //forse da problemi
        return reader;
    }
    public DataTable ReadTable(string query, params QueryParameter[] queryParams)
    {
        SqlCommand command = GetCommand(query, queryParams);
        DataTable datatable = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        adapter.Fill(datatable);
        command.Dispose();
        return datatable;
    }

    public void Write(string query, params QueryParameter[] queryParams)//insert, update, delete, ecc..
    {
        SqlCommand command = GetCommand(query, queryParams);
        command.ExecuteNonQuery(); //o qualcosa del genere
        //...
    }

    public void Close()
    {
        _conn.Close();
    }
}
public class NomeTabellaNelDb
{
    private DbConnection _conn;
    public NomeTabellaNelDb(DbConnection conn)
    {
        _conn = conn;
    }
    public void InserisciPersona(string nome, int id)
    {
        string query = "INSERT INTO NomeTabellaNelDb... {0} ...";
        _conn.Write(query,
                    new QueryParameter() { Name = "nome", Value = nome, Type = SqlDbType.VarChar },
                    new QueryParameter() { Name = "id", Value = id, Type = SqlDbType.Int });
    }
}

public class DbReader
{
    private string _query;
    private string _connectionString;
    private SqlConnection _conn;
    private SqlCommand _command;
    private SqlDataReader _reader;
    private SqlDataAdapter _adapter;
    private DataTable _datatable;


    public DbReader(string connectionString, string query)
    {
        try
        {
            _query = query;
            _connectionString = connectionString;
            _conn = new SqlConnection(_connectionString);
            _conn.Open();
            _command = new SqlCommand(_query, _conn);
            _adapter = null;
            _datatable = new DataTable();
            _reader = null;
        }
        catch { }
    }

    public DataTable GetDataTable()
    {
        try
        {
            _adapter = new SqlDataAdapter(_command);
            _adapter.Fill(_datatable);
            return _datatable;
        }
        catch { return null; }
    }

    public SqlDataReader GetDataReader()
    {
        try
        {
            _reader = _command.ExecuteReader();
        }
        catch { return null; }
        return _reader;
    }

    public void SetParam(string name, SqlDbType type, string value)
    {
        _command.Parameters.Add("@" + name, type);
        _command.Parameters["@" + name].Value = value;
    }

    public void SetQuery(string query)
    {
        _query = query;
        _command = new SqlCommand(_query, _conn);
        _reader.Dispose();
    }

    public string GetQuery()
    {
        return _query;
    }

    public void Close()
    {
        _reader.Close();
        _command.Dispose();
        _conn.Close();
    }
}