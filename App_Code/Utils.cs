﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for Utils
/// </summary>
public class Utils
{
    public static string MacToString(System.Data.Linq.Binary bytes)
    {
        try
        {
            List<byte> list = new List<byte>(bytes.ToArray());
            StringBuilder mac = new StringBuilder();
            foreach (byte b in list)
            {
                mac.Append(b.ToString("X2"));
                mac.Append(":");
            }
            return mac.Remove(mac.Length - 1, 1).ToString();
        }
        catch
        {
            return "NO IP";
        }

    }

    public static string IpToString(NetworkAddress address)
    {
        if (address == null) return "";
        if (address.Ipv4 != null) return Ipv4ToString(address.Ipv4);
        if (address.Ipv6 != null) return Ipv6ToString(address.Ipv6);
        return "";
    }
    public static string Ipv4ToString(System.Data.Linq.Binary bytes)
    {
        
            List<byte> list = new List<byte>(bytes.ToArray());
            StringBuilder mac = new StringBuilder();
            foreach (byte b in list)
            {
                mac.Append(b.ToString());
                mac.Append(".");
            }
            return mac.Remove(mac.Length - 1, 1).ToString();
       
    }
    public static string Ipv6ToString(System.Data.Linq.Binary bytes)
    {
        
            List<byte> list = new List<byte>(bytes.ToArray());
            StringBuilder mac = new StringBuilder();
            int c = 0;
            foreach (byte b in list)
            {
                mac.Append(b.ToString("X2"));
                c++;
                if (c % 2 == 0) mac.Append(":");
            }
            return mac.Remove(mac.Length - 1, 1).ToString();
       
    }
    public static bool IsInteger(string value)
    {
        int a;
        return int.TryParse(value, out a);
    }
}