﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="hosts.aspx.cs" Inherits="hosts" %>

<asp:Content ID="Content" ContentPlaceHolderID="hosts" runat="Server">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="js/tables.js"></script>
    <script type="text/javascript">
        //google.charts.setOnLoadCallback(drawTable);
        function drawTable() {
            drawHostsTable();
        }
    </script>
    <div class="container">
        <asp:LinqDataSource ID="LINQdatasource" runat="server" ContextTypeName="DataClassesNetStatsDataContext" TableName="Hosts" EnableUpdate="true" OnSelecting="LINQdatasource_Selecting"></asp:LinqDataSource>
        <div id="tbl-container">
            <asp:GridView ID="grd1" runat="server" DataKeyNames="Id, CreationDate, MAC, Manufacturer" DataSourceID="LINQdatasource" CssClass="table table-striped table-hover" GridLines="None" AutoGenerateColumns="false" AllowSorting="true" AutoGenerateEditButton="true" AllowPaging="false" PageSize="100">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Host id" ReadOnly="true" SortExpression="Id" />
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="CreationDate" HeaderText="Creation date" ReadOnly="true" SortExpression="CreationDate" />
                    <asp:BoundField DataField="MAC" HeaderText="MAC" ReadOnly="true" SortExpression="machidden" />
                    <asp:BoundField DataField="Manufacturer" HeaderText="Manufacturer name" ReadOnly="true" SortExpression="Manufacturer" />
                    <asp:BoundField DataField="Ip" HeaderText="Ip address" ReadOnly="true" SortExpression="iphidden" />
                    <asp:TemplateField>
                        <HeaderTemplate><a href="">More informations..</a></HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="btnInfo" runat="server" CommandArgument='<%#Eval("Id")%>' OnCommand="btnInformation_Command">More info.. <i class="fa fa-external-link" aria-hidden="true"></i>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
        <!--
        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <h1>Hosts</h1>
                <div id="hosts_table" ></div>
            </div>
        </div>
        -->
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="StylesheetsSpecific" runat="Server">
    <link rel="stylesheet" href="css/pager.css" type="text/css" />
    <link rel="stylesheet" href="css/grid-slider.css" type="text/css" />
</asp:Content>

