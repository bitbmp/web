﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class hosts : System.Web.UI.Page
{
    DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void LINQdatasource_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        var query = from h in db.Hosts
                    where h.Id != 0
                    select new { h.Id, h.Name, h.CreationDate, MAC = Utils.MacToString(h.MacAddress), machidden = h.MacAddress, Manufacturer = h.Manufacturer.Name, iphidden = h.Bindings.ToList().First(bind => !bind.StopDate.HasValue).NetworkAddress.Ipv4, Ip = Utils.IpToString(h.Bindings.ToList().First(bind => !bind.StopDate.HasValue).NetworkAddress) };
        e.Result = query;
    }
    

    protected void btnInformation_Command(object sender, CommandEventArgs e)
    {
        Response.Redirect("stats.aspx?id=" + e.CommandArgument.ToString());
    }

    [WebMethod]
    public static List<object> GetLastQueryDnsTableData(string id, int limit)
    {
        List<object> chartData = new List<object>();

        DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
        var query = from h in db.Hosts
                    select new {
                        Id = h.Id,
                        Name = h.Name,
                        Date = h.CreationDate,
                        MAC = Utils.MacToString(h.MacAddress),
                        Manufacturer = h.Manufacturer.Name,
                        Ip = Utils.IpToString(h.Bindings.ToList().First(bind => !bind.StopDate.HasValue).NetworkAddress) };
   
        chartData.Add(new object[]
        {
            "ID" , "HOST NAME", "CREATION DATE", "MAC", "IP", "MANUFACTURER"
        });
        foreach (var r in query)
        {
            chartData.Add(new object[]
            {
                r.Id, r.Name, r.Date.ToString(), r.MAC, r.Ip, r.Manufacturer
            });
        }

        return chartData;

    }
}