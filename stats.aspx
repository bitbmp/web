﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="stats.aspx.cs" Inherits="stats" %>



<asp:Content ID="Content" ContentPlaceHolderID="hosts" runat="Server">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="js/charts.js"></script>
    <script type="text/javascript" src="js/tables.js"></script>
    <script type="text/javascript">

        $(".google-visualization-table-table")
            .removeClass('google-visualization-table-table')
            .addClass('table table-bordered table-condensed table-striped');
        google.charts.setOnLoadCallback(drawAll);

        setInterval(function () { drawSpeedGraph1m(<%=this.idHost%>); }, 1000);
        setInterval(function () { drawSpeedGraph24h(<%=this.idHost%>); }, 1000 * 60);
        setInterval(function () { drawCurrentNetowrkUsage(<%=this.idHost%>); }, 1000);


        function drawAll() {
            drawBindingsTable(<%=this.idHost%>);
            drawLastContacts(<%=this.idHost%>);
            drawLastVisitedSitesTable(<%=this.idHost%>);
            drawLastQueryDnsTable(<%=this.idHost%>);

            drawSpeedGraph24h(<%=this.idHost%>);
            drawSpeedGraph1m(<%=this.idHost%>);
            drawPiePort(<%=this.idHost%>);
        }


    </script>


    <div class="container">

        <div class="row">
            <div class="col-lg-10 col-sx-12">
                <div class="form-group">
                    <asp:TextBox Style="width: 100%" runat="server" ID="txbHost" CssClass="form-control" placeholder="(MAC) 2C:4D:54:D1:50:11 / (Name) My-PC / (IP) 192.168.0.35 / (ID) 43"></asp:TextBox>
                </div>

            </div>
            <div class="col-lg-2 col-sx-12">
                <asp:Button runat="server" ID="btnStats" Text="View Stats" CssClass="btn btn-default" OnClick="btnStats_Click"></asp:Button>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <h3>Bindings</h3>
                <div id="binding_table"></div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <h3>Network usage</h3>
                <div id="tbl-container">
                    <div id="curvechart1m" style="width: 100%; height: 500px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <p id="network_usage" style="text-align: center;"></p>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <div id="tbl-container">
                    <div id="curvechart24h" style="width: 100%; height: 500px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <p id="network_usage_total" style="text-align: center;"></p>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <h3>Port usage</h3>
                <div id="tbl-container">
                    <div id="piechart" style="width: 100%; height: 500px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <h3><a style="color: black;" href="javascript:drawLastContacts('<%=this.idHost%>' ,5);">Last contacts</a></h3>
                <div id="last_contacts_table"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <h3><a style="color: black;" href="javascript:drawLastVisitedSitesTable('<%=this.idHost%>' ,5);">Last visited sites</a></h3>
                <div id="last_visited_sites"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <h3><a style="color: black;" href="javascript:drawLastQueryDnsTable('<%=this.idHost%>' ,5);">Last query dns</a></h3>
                <div id="last_query_dns"></div>
            </div>
        </div>

    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="StylesheetsSpecific" runat="Server">
    <link rel="stylesheet" href="css/grid-slider.css" type="text/css" />
</asp:Content>
