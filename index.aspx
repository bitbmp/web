﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" MasterPageFile="~/main.master" %>

<asp:Content ID="Content" ContentPlaceHolderID="index" Runat="Server">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="js/charts.js"></script>
    <script type="text/javascript" src="js/tables.js"></script>
    <script type="text/javascript">

        var sec_span = 300;
        setInterval(function () { drawSpeedGraph1m(0, sec_span); drawCurrentNetowrkUsage(0);}, 1000);
        google.charts.setOnLoadCallback(function () { drawSpeedGraph1m(0, sec_span); drawCurrentNetowrkUsage(0); });

        
    </script>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sx-12">
                <div id="curvechart1m" style="width: 100%; height:600px;"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-sx-12">
            <p id="network_usage" style="text-align: center;"></p>
        </div>
    </div>
    <div class="row">
            <div class="col-lg-12 col-sx-12">
                </p><p id="network_usage_total" style="text-align: center;"></p>
            </div>
        </div>

</asp:Content>

