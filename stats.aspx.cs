﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class stats : System.Web.UI.Page
{
    private const int LimitHttp = 5, LimitDns = 5, LimitContants = 5;

    private DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
    protected int? idHost = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            txbHost.Text = Request.QueryString["id"];
        if (string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            idHost = null;
        }
        else if (!string.IsNullOrEmpty(Request.QueryString["id"]) && Utils.IsInteger(Request.QueryString["id"]))
        {
            idHost = int.Parse(Request.QueryString["id"]);
        }
        else
        {
            Host h = (from hosts in db.Hosts select hosts).ToList().Where(it => Utils.MacToString(it.MacAddress).Equals(Request.QueryString["id"])).FirstOrDefault();



            if (h == null)
            {
                h = db.Hosts.Where(it => it.Name.Equals(Request.QueryString["id"])).FirstOrDefault();
                if (h == null)
                {
                    Binding b = (from hosts in db.Bindings where hosts.StopDate == null select hosts).ToList().Where(it => Utils.IpToString(it.NetworkAddress).Equals(Request.QueryString["id"])).FirstOrDefault();
                    if (b == null)
                    {
                        txbHost.Text = "";
                        idHost = null;
                    }
                    else
                    {
                        idHost = b.Host.Id;
                    }
                }
                else
                {
                    idHost = h.Id;
                }
            }
            else
            {
                idHost = h.Id;
            }
        }
    }
    protected void btnStats_Click(object sender, EventArgs e)
    {
        Response.Redirect("stats.aspx?id=" + txbHost.Text);
    }


    [WebMethod]
    public static List<object> GetChartPortsData(string id)
    {
        List<object> chartData = new List<object>();

        chartData.Add(new object[]
        {
            "Port Number", "Total Usage"
        });

        DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
        var ports = from r in db.Contacts
                    group r by r.PortNumber into nr
                    select new { Port = nr.Key, ContactsCount = nr.Sum(r => r.ContactsCount) };
        if (Utils.IsInteger(id))
        {
            ports = from r in db.Contacts
                    where r.IdHost == int.Parse(id)
                    group r by r.PortNumber into nr
                    select new { Port = nr.Key, ContactsCount = nr.Sum(r => r.ContactsCount) };
        }


        var topPorts = (from r in ports orderby r.ContactsCount descending select r).Take(4);

        ///TODO: Extremely inefficient
        var listPorts = ports.ToList();
        var listTopPorts = topPorts.ToList();
        chartData.Add(new object[]
           {
                "Other", listPorts.Where(it => listTopPorts.All(t => it.Port != t.Port)).Sum(it => it.ContactsCount)
           });


        foreach (var r in topPorts)
        {
            chartData.Add(new object[]
            {
                r.Port.ToString(), r.ContactsCount
            });

        }


        return chartData;

    }
    [WebMethod]
    public static List<object> GetChartSpeedsDataHour(string id, int range_seconds)
    {
        List<object> chartData = new List<object>();
        chartData.Add(new object[]
        {
            "Date", "Download", "Upload"
        });

        DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
        DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0);
        if (!Utils.IsInteger(id))
        {
            id = "0";
        }

        var query = from r in db.HourStats
                    where r.IdHost == int.Parse(id) && (now - r.Date).TotalSeconds <= range_seconds
                    orderby r.Date ascending
                    select new GraphRowRecord { Date = r.Date, Upload = r.UploadedBytes, Download = r.DownloadedBytes };

        List<GraphRowValue> data = new List<GraphRowValue>();
        SpeedFromTotalToDelta(query, data);
        Compensate(data, TimeSpan.FromSeconds(60 * 60), TimeSpan.FromSeconds(range_seconds), now);
        ConvertGraphRowValueToGoogleChart(data, chartData);
        return chartData;
    }
    [WebMethod]
    public static List<object> GetChartSpeedsDataMinute(string id, int range_seconds)
    {
        List<object> chartData = new List<object>();
        chartData.Add(new object[]
        {
            "Date", "Download", "Upload"
        });

        DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
        DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0);
        if (!Utils.IsInteger(id))
        {
            id = "0";
        }

        var query = from r in db.MinStats
                    where r.IdHost == int.Parse(id) && (now - r.Date).TotalSeconds <= range_seconds
                    orderby r.Date ascending
                    select new GraphRowRecord { Date = r.Date, Upload = r.UploadedBytes, Download = r.DownloadedBytes };

        List<GraphRowValue> data = new List<GraphRowValue>();
        SpeedFromTotalToDelta(query, data);
        Compensate(data, TimeSpan.FromSeconds(60), TimeSpan.FromSeconds(range_seconds), now);
        ConvertGraphRowValueToGoogleChart(data, chartData);
        return chartData;
    }
    [WebMethod]
    public static List<object> GetChartSpeedsDataSeconds(string id, int range_seconds)
    {
        List<object> chartData = new List<object>();
        chartData.Add(new object[]
        {
            "Date", "Download", "Upload"
        });

        DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
        DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        if (!Utils.IsInteger(id))
        {
            id = "0";
        }

        var query = from r in db.SecStats
                    where r.IdHost == int.Parse(id) && (now - r.Date).TotalSeconds <= range_seconds
                    orderby r.Date ascending
                    select new GraphRowRecord { Date = r.Date, Upload = r.UploadedBytes, Download = r.DownloadedBytes };

        List<GraphRowValue> data = new List<GraphRowValue>();
        SpeedFromTotalToDelta(query, data);
        Compensate(data, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(range_seconds), now);
        ConvertGraphRowValueToGoogleChart(data, chartData, "hh:mm:ss");
        return chartData;
    }
    private static void Compensate(List<GraphRowValue> data, TimeSpan inc, TimeSpan range, DateTime now)
    {
        if (data.Count == 0)
            for (DateTime i = now.Subtract(range); i <= now; i = i.Add(inc))
                data.Add(new GraphRowValue() { Date = i });
        else
        {
            int k = 0;
            for (DateTime i = now.Subtract(range); i <= now; i = i.Add(inc))
            {
                if(k >= data.Count)
                {
                    data.Add(new GraphRowValue() { Date = i });
                }
                else if (data[k].Date > i)
                {
                    data.Insert(k, new GraphRowValue() { Date = i });
                    k++;
                }
                else if(data[k].Date < i)
                {
                    while(k < data.Count && data[k].Date < i)
                    {
                        k++;
                    }
                    i = i.Subtract(inc);
                }
                else
                {
                    k++;
                }
            }
        }
    }

    [WebMethod]
    public static List<object> GetBindingTableData(string id, int limit)
    {
        List<object> chartData = new List<object>();

        DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
        var query = (from b in db.Bindings
                     orderby b.IdHost, b.StopDate ascending, b.StartDate descending
                     select new
                     {
                         Host = HostToString(b.Host),
                         Ip = Utils.IpToString(b.NetworkAddress),
                         Date = b.StopDate,
                     });
        if (Utils.IsInteger(id))
        {
            query = (from b in db.Bindings
                     where b.Host.Id == int.Parse(id)
                     orderby b.IdHost, b.StopDate ascending, b.StartDate descending
                     select new
                     {
                         Host = HostToString(b.Host),
                         Ip = Utils.IpToString(b.NetworkAddress),
                         Date = b.StopDate,
                     });
        }

        chartData.Add(new object[]
        {
            "HOST", "IP", "EXPIRED IN"
        });
        foreach (var r in query)
        {
            chartData.Add(new object[]
            {
                r.Host, r.Ip, r.Date.ToString()
            });
        }

        return chartData;

    }
    [WebMethod]
    public static List<object> GetLastContactsTableData(string id, int limit)
    {
        List<object> chartData = new List<object>();

        DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
        var query = (from c in db.Contacts
                     orderby c.LastVisit descending
                     select new
                     {
                         Host = HostToString(c.Host),
                         Ip = Utils.IpToString(c.NetworkAddress),
                         Date = c.LastVisit,
                         Port = c.PortNumber,
                         Count = c.ContactsCount
                     }).Take(limit);
        if (Utils.IsInteger(id))
        {
            query = (from c in db.Contacts
                     where c.Host.Id == int.Parse(id)
                     orderby c.LastVisit descending
                     select new
                     {
                         Host = HostToString(c.Host),
                         Ip = Utils.IpToString(c.NetworkAddress),
                         Date = c.LastVisit,
                         Port = c.PortNumber,
                         Count = c.ContactsCount
                     }).Take(limit);
        }

        chartData.Add(new object[]
        {
            "HOST", "SERVER", "PORT", "DATE", "COUNT"
        });
        foreach (var r in query)
        {
            chartData.Add(new object[]
            {
                r.Host, r.Ip, r.Port, r.Date.ToString(), r.Count
            });
        }

        return chartData;

    }
    [WebMethod]
    public static List<object> GetLastVisitedSitesTableData(string id, int limit)
    {
        List<object> chartData = new List<object>();

        DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
        var query = (from h in db.HTTPs
                     orderby h.Date descending
                     select new { Host = HostToString(h.Host), h.URL, h.IsSecure, h.Date }).Take(limit);
        if (Utils.IsInteger(id))
        {
            query = (from h in db.HTTPs
                     where h.Host.Id == int.Parse(id)
                     orderby h.Date descending
                     select new { Host = HostToString(h.Host), h.URL, h.IsSecure, h.Date }).Take(limit);
        }

        chartData.Add(new object[]
        {
            "HOST", "URL", "DATE", "HTTPS"
        });
        foreach (var r in query)
        {
            chartData.Add(new object[]
            {
                r.Host, r.URL, r.Date.ToString(), false
            });
        }

        return chartData;

    }
    [WebMethod]
    public static List<object> GetLastQueryDnsTableData(string id, int limit)
    {
        List<object> chartData = new List<object>();

        DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
        var query = (from d in db.DnsQueries
                     orderby d.Date descending
                     select new
                     {
                         Host = HostToString(d.Host),
                         Ip = Utils.IpToString(d.NetworkAddress),
                         d.Date,
                         Q = DnsQuestionsToString(d.DnsQuestionSections),
                         A = DnsAnswersToString(d.DnsResourceRecordCNAMEs, d.DnsResourceRecordAs),
                         TTL = DnsAnswersToStringTTL(d.DnsResourceRecordCNAMEs, d.DnsResourceRecordAs)
                     }).Take(limit);
        if (Utils.IsInteger(id))
        {
            query = (from d in db.DnsQueries
                     where d.Host.Id == int.Parse(id)
                     orderby d.Date descending
                     select new
                     {
                         Host = HostToString(d.Host),
                         Ip = Utils.IpToString(d.NetworkAddress),
                         d.Date,
                         Q = DnsQuestionsToString(d.DnsQuestionSections),
                         A = DnsAnswersToString(d.DnsResourceRecordCNAMEs, d.DnsResourceRecordAs),
                         TTL = DnsAnswersToStringTTL(d.DnsResourceRecordCNAMEs, d.DnsResourceRecordAs)
                     }).Take(limit);
        }

        chartData.Add(new object[]
        {
            "HOST", "SERVER", "DATE", "QUESTIONS", "ANSWERS", "TTL"
        });
        foreach (var r in query)
        {
            chartData.Add(new object[]
            {
                r.Host, r.Ip, r.Date.ToString(), r.Q, r.A, r.TTL
            });
        }

        return chartData;

    }

    [WebMethod]
    public static List<object> GetCurrentNetworkUsage(string id)
    {
        List<object> chartData = new List<object>();

        DataClassesNetStatsDataContext db = new DataClassesNetStatsDataContext();
        DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        if (!Utils.IsInteger(id))
        {
            id = "0";
        }

        int iid = int.Parse(id);
        DateTime lastDate = db.SecStats.Where(it => it.IdHost == iid).Max(r => r.Date);
        var query = (from r in db.SecStats
                     where r.IdHost == iid
                     orderby r.Date descending
                     select new GraphRowRecord { Date = r.Date, Upload = r.UploadedBytes, Download = r.DownloadedBytes }).Take(2).ToList();

        if (query.Count < 2) return chartData;
        double down = (query[0].Download - query[1].Download) / (query[0].Date - query[1].Date).TotalSeconds;
        double up = (query[0].Upload - query[1].Upload) / (query[0].Date - query[1].Date).TotalSeconds;

        chartData.Add(down);
        chartData.Add(up);
        chartData.Add(query[0].Download);
        chartData.Add(query[0].Upload);

        return chartData;
    }


    private struct GraphRowRecord
    {
        public DateTime Date;
        public long Download, Upload;
    }
    private struct GraphRowValue
    {
        public DateTime Date;
        public double DDownload, DUpload;
    }
    private static void SpeedFromTotalToDelta(IQueryable<GraphRowRecord> query, List<GraphRowValue> chartData)
    {
        long lastUp = -1, lastDown = -1;
        DateTime lastDateTime = DateTime.Now;
        foreach (var r in query)
        {
            if (lastDown == -1)
            {
                lastUp = r.Upload;
                lastDown = r.Download;
                lastDateTime = r.Date;
                continue;
            }
            double sec = (r.Date - lastDateTime).TotalSeconds;
            chartData.Add(new GraphRowValue
            {
                Date = r.Date,
                DDownload = (r.Download - lastDown) / sec,
                DUpload = (r.Upload - lastUp) / sec
            });
            lastUp = r.Upload;
            lastDown = r.Download;
            lastDateTime = r.Date;
        }
    }
    private static void ConvertGraphRowValueToGoogleChart(List<GraphRowValue> valueIn, List<object> valueOut, string dataFormat = "hh:mm")
    {
        for (int i = 0; i < valueIn.Count; i++)
        {
            valueOut.Add(new object[] { valueIn[i].Date.ToString(dataFormat), valueIn[i].DDownload * 8 / 1000000.0, valueIn[i].DUpload * 8 / 1000000.0 });
        }
    }
    private static string HostToString(Host host)
    {
        return (string.IsNullOrEmpty(host.Name)) ? Utils.MacToString(host.MacAddress) : host.Name;
    }
    private static string DnsQuestionsToString(EntitySet<DnsQuestionSection> set)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < set.Count; i++)
        {
            sb.Append(set[i].QName);
            sb.Append("<br/>");
        }

        return sb.ToString();
    }
    private static string DnsAnswersToString(EntitySet<DnsResourceRecordCNAME> setCNAME, EntitySet<DnsResourceRecordA> setA)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < setCNAME.Count; i++)
        {
            sb.Append(setCNAME[i].Name);
            sb.Append(" &rarr; ");
            sb.Append(setCNAME[i].Value);
            sb.Append("<br/>");
        }

        for (int i = 0; i < setA.Count; i++)
        {
            sb.Append(setA[i].Name);
            sb.Append(" &rarr; ");
            sb.Append(Utils.IpToString(setA[i].NetworkAddress));
            sb.Append("<br/>");
        }
        return sb.ToString();
    }
    private static string DnsAnswersToStringTTL(EntitySet<DnsResourceRecordCNAME> setCNAME, EntitySet<DnsResourceRecordA> setA)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < setCNAME.Count; i++)
        {
            sb.Append(TimeSpan.FromSeconds(setCNAME[i].TTL));
            sb.Append("<br/>");
        }

        for (int i = 0; i < setA.Count; i++)
        {
            sb.Append(TimeSpan.FromSeconds(setA[i].TTL));
            sb.Append("<br/>");
        }
        return sb.ToString();
    }
}